package es.unex.politecnica.spilab.csprofile.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import es.unex.politecnica.spilab.csprofile.MainActivity;
import es.unex.politecnica.spilab.csprofile.R;
import es.unex.politecnica.spilab.csprofile.adapters.CustomListAdapterSkill;
import es.unex.politecnica.spilab.csprofile.models.Skill;

public class SkillsFragment extends Fragment {

    private ArrayList<Skill> skills;
    private CustomListAdapterSkill customListAdapterSkill;
    private ListView customListView;
    private FloatingActionButton fab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_skills, viewGroup, false);

        customListView = (ListView) v.findViewById(R.id.custom_list_view_s);
        registerForContextMenu(customListView);
        skills = new ArrayList<>(MainActivity.fullProfile.getSkills());
        customListAdapterSkill = new CustomListAdapterSkill(skills, getContext());
        customListView.setAdapter(customListAdapterSkill);
        //getDatas();
        customListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(MainActivity.this, "Name : " + names[i] + "\n Profession : " + professions[i], Toast.LENGTH_SHORT).show();
            }
        });

        // floating button
        fab = v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Dialog add skill
                final View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_skill, null);
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setView(v);
                builder1.setCancelable(true);
                builder1.setTitle("New skill");
                final EditText nameText = v.findViewById(R.id.nameText);
                final EditText valueText = v.findViewById(R.id.valueText);
                final EditText endpointText = v.findViewById(R.id.endpointText);
                builder1.setPositiveButton(
                        "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String name = nameText.getText().toString();
                                String desiredValue = valueText.getText().toString();
                                String endpoint = endpointText.getText().toString();
                                Skill s = new Skill(name, desiredValue, endpoint);
                                customListAdapterSkill.addSkill(s);

                                MainActivity.fullProfile.getSkills().add(s);
                                MainActivity.writeFileExternalStorage();

                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();


            }
        });


        return v;
    }

    // getting all the datas
    private void getDatas() {
        for (int i = 0; i < 5; i++) {
            //skills.add(new Skill(names[count], professions[count], photos[count]));
            skills.add(new Skill("Skill" + i, "value = " + i, "endp = " + i));

        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.custom_list_view_s) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_skill, menu);
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit_s:
                // Dialog add skill
                final View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_skill, null);
                //View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_new_goal, null);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setView(v);
                builder1.setCancelable(true);
                builder1.setTitle("Edit skill");

                String name = skills.get(info.position).getName();
                String currentValue = skills.get(info.position).getCurrentValue();
                final String endpointValue = skills.get(info.position).getEndpoint();

                final EditText nameText = v.findViewById(R.id.nameText);
                nameText.setText(name);
                final EditText valueText = v.findViewById(R.id.valueText);
                valueText.setText(currentValue);
                final EditText endpointText = v.findViewById(R.id.endpointText);
                endpointText.setText(endpointValue);
                builder1.setPositiveButton(
                        "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String name = nameText.getText().toString();
                                String desiredValue = valueText.getText().toString();
                                String endpointValue = endpointText.getText().toString();

                                Skill s = new Skill(name, desiredValue, endpointValue);
                                customListAdapterSkill.editSkill(s, info.position);

                                MainActivity.fullProfile.getSkills().get(info.position).setName(name);
                                MainActivity.fullProfile.getSkills().get(info.position).setCurrentValue(desiredValue);
                                MainActivity.fullProfile.getSkills().get(info.position).setEndpoint(endpointValue);
                                MainActivity.writeFileExternalStorage();

                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
                return true;
            case R.id.delete_s:
                customListAdapterSkill.deleteSkill(info.position);
                MainActivity.fullProfile.getSkills().remove(info.position);
                MainActivity.writeFileExternalStorage();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
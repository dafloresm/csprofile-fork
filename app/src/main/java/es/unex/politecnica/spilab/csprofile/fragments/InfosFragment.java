package es.unex.politecnica.spilab.csprofile.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import es.unex.politecnica.spilab.csprofile.MainActivity;
import es.unex.politecnica.spilab.csprofile.R;
import es.unex.politecnica.spilab.csprofile.adapters.CustomListAdapterInfo;
import es.unex.politecnica.spilab.csprofile.models.Info;

public class InfosFragment extends Fragment {

    private ArrayList<Info> infos;
    private CustomListAdapterInfo customListAdapterInfo;
    private ListView customListView;
    private FloatingActionButton fab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_infos, viewGroup, false);

        customListView = (ListView) v.findViewById(R.id.custom_list_view_i);
        registerForContextMenu(customListView);
        infos = new ArrayList<>(MainActivity.fullProfile.getInfos());
        customListAdapterInfo = new CustomListAdapterInfo(infos, getContext());
        customListView.setAdapter(customListAdapterInfo);
        //getDatas();
        customListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(MainActivity.this, "Name : " + keys[i] + "\n Profession : " + professions[i], Toast.LENGTH_SHORT).show();
            }
        });

//        // floating button
//        fab = v.findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // Dialog add info
//                final View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_info, null);
//                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
//                builder1.setView(v);
//                builder1.setCancelable(true);
//                builder1.setTitle("New info");
//                final EditText keyText = v.findViewById(R.id.keyText);
//                final EditText valueText = v.findViewById(R.id.valueText);
//                builder1.setPositiveButton(
//                        "Save",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                String key = keyText.getText().toString();
//                                String value = valueText.getText().toString();
//                                Info g = new Info(key, value);
//                                customListAdapterInfo.addInfo(g);
//                            }
//                        });
//
//                builder1.setNegativeButton(
//                        "Cancel",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.cancel();
//                            }
//                        });
//
//                AlertDialog alert11 = builder1.create();
//                alert11.show();
//
//
//            }
//        });


        return v;
    }

    // getting all the datas
    private void getDatas() {
/*        for (int i = 0; i < 5; i++) {
            //infos.add(new Info(keys[count], professions[count], photos[count]));
            infos.add(new Info("Info" + i, "value = " + i));

        }*/

        infos = new ArrayList<>(MainActivity.fullProfile.getInfos());

        Log.d("TEST-Infos", "Infos = " + this.infos);

    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.custom_list_view_i) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.menu_info, menu);
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.edit_i:
                // Dialog add info
                final View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_info, null);
                //View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_new_info, null);

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setView(v);
                builder1.setCancelable(true);
                builder1.setTitle("Edit info");

                String key = infos.get(info.position).getKey();
                String value = infos.get(info.position).getValue();

                final TextView keyText = v.findViewById(R.id.keyText);
                keyText.setText(key);
                final EditText valueText = v.findViewById(R.id.valueText);
                valueText.setText(value);
                builder1.setPositiveButton(
                        "Save",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String key = keyText.getText().toString();
                                String value = valueText.getText().toString();
                                Info g = new Info(key, value);
                                customListAdapterInfo.editInfo(g, info.position);

                                MainActivity.fullProfile.getInfos().get(info.position).setValue(value);
                                MainActivity.writeFileExternalStorage();

                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
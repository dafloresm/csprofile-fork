#Table of content

[TOC]

##Information
**CSProfile** manages people virtual profiles to be offered to the server by using MQTT protocol (Check the [HassioCS](https://bitbucket.org/spilab/server-node-python/src) Readme.md).

![](https://bitbucket.org/spilab/android/raw/ab66703f3775f6a4db4757e0162888bc7faaa916/images/CSProfile1.jpg)  | ![](https://bitbucket.org/spilab/android/raw/ab66703f3775f6a4db4757e0162888bc7faaa916/images/CSProfile2.jpg)
------------- | -------------
|

##Get started
Just clone the repository and compile the apk

`$ git clone https://dafloresm@bitbucket.org/spilab/android.git`

###Requirements
Smartphone with Android 8.0 or higher.

##How to use
###Installation
 1. Compile and install the application.
 2. Run it!

##Usage
There are 4 main tabs:

 1. **Config**: to set server information. Just configure the IP. Note: the smartphone and the server must be within the same network. In this tab the profile can also be sended manually in case the MQTT server does not be able to request it.

 2. **Info**: to show the profile information such as the ID, name, IP and MAC addresses.

 3. **Goals**: to specify the different goals.

 4. **Skills**: to specify the different skills.

#Contact
dfloresm@unex.es